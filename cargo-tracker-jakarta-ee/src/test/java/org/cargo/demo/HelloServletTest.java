package org.cargo.demo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class HelloServletTest {
    @Mock private HttpServletRequest mockRequest;
    @Mock private HttpServletResponse mockResponse;
    @Mock private RequestDispatcher mockRequestDispatcher;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void doGet() throws Exception {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        when(mockResponse.getWriter()).thenReturn(printWriter);

        new HelloServlet().doGet(mockRequest, mockResponse);

        assertEquals("Hello, World!", stringWriter.toString());
    }

    @Test
    public void doPostWithoutName() throws Exception {
        when(mockRequest.getRequestDispatcher("response.jsp"))
            .thenReturn(mockRequestDispatcher);

        new HelloServlet().doPost(mockRequest, mockResponse);

        verify(mockRequest).setAttribute("user", "World");
        verify(mockRequestDispatcher).forward(mockRequest,mockResponse);
    }

    @Test
    public void doPostWithName() throws Exception {
        when(mockRequest.getParameter("name")).thenReturn("Dolly");
        when(mockRequest.getRequestDispatcher("response.jsp"))
            .thenReturn(mockRequestDispatcher);

        new HelloServlet().doPost(mockRequest, mockResponse);

        verify(mockRequest).setAttribute("user", "Dolly");
        verify(mockRequestDispatcher).forward(mockRequest,mockResponse);
    }
}
